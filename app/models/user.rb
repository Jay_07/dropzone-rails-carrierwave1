class User < ActiveRecord::Base
		mount_uploader :image, ImageUploader
	validate :image_size_validation
  	validates :image ,
				:presence => true

  private
  
  def image_size_validation
    errors[:image] << "should be less than 4MB" if image.size > 4.megabytes
  end

end
